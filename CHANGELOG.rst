Changelog
=========

1.2 (2023-07-13)
--------------------

* Added the Capon beamformer, a bit experimental still
* Added the unaliasing Doppler method, no unit tests
* Fixes for 3D Doppler
* Miscellaneous fixes

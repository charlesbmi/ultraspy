.. _tech_choices:

Technical choices
=================
This section is describing, a bit in a mess, multiple technical choices that
has been made.


.. toctree::
   :maxdepth: 1

   beamforming_choices
   gpu_configs
   gpu_beamformers
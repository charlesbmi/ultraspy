`ultraspy` classes
==================

Probe classes
-------------

.. autoclass:: ultraspy.probes.probe.Probe
   :members:

.. tabs::

    .. group-tab:: Linear

        .. autoclass:: ultraspy.probes.linear_probe.LinearProbe
           :members:

    .. group-tab:: Convex

        .. autoclass:: ultraspy.probes.convex_probe.ConvexProbe
           :members:

    .. group-tab:: Matricial

        .. autoclass:: ultraspy.probes.matricial_probe.MatricialProbe
           :members:


Scan classes
------------

.. autoclass:: ultraspy.scan.Scan
   :members:

.. tabs::

    .. group-tab:: Regular grid

        .. autoclass:: ultraspy.scan.GridScan
           :members:

    .. group-tab:: Polar grid

        .. autoclass:: ultraspy.scan.PolarScan
           :members:


Reader class
------------

.. autoclass:: ultraspy.io.reader.Reader
   :members:

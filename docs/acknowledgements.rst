.. _thanks:

Acknowledgements
================

ANR LabCom
----------
This work has been done as a part of the ANR LabCom Image4US between
`CREATIS <https://www.creatis.insa-lyon.fr>`_ (medical imaging research lab)
and `TPAC <https://thephasedarraycompany.com>`_ (electronics for ultrasound
scanners, specialized in NDT). The project is willing to build the next gen
research ultrasound system, with 1024 channels, allowing for 3D imaging at high
PRF.


Technical contributors
----------------------
- Pierre Ecarlat: pierre.ecarlat@gmail.com

- Fabien Milloz: fabien.milloz@univ-lyon1.fr


Supervision
-----------
The project is under the supervision of:

- Barbara Nicolas (CREATIS): barbara.nicolas@creatis.insa-lyon.fr

- Ewen Carcreff (TPAC / DB-SAS): ewen.carcreff@aos-ndt.com


The library development involves the following persons: Pierre Ecarlat
(Creatis), Barbara Nicolas (Creatis), Ewen Carcreff (TPAC), Hervé Liebgott
(Creatis), Francois Varray (Creatis), Damien Garcia  (Creatis) and Fabien
Milloz (Creatis).


Materials
---------
The material used for the experimentation is provided by the TPAC company and
the `PILoT facility <https://www.creatis.insa-lyon.fr/site7/en/PILoT>`_
(INSA-Lyon).

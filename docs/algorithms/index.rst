.. _algorithms:

Algorithms
==========
This section provides a description of the proposed algorithms.


.. toctree::
   :maxdepth: 1

   das_algo
   fdmas_algo
   pdas_algo
   capon_algo

format long


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load file and convert to I/Qs

load('PWI_disk.mat');
param.fnumber = 1;
IQ = rf2iq(RF, param);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Grid of interest / beamforming

x = linspace(-12.5e-3, 12.5e-3, 251);
z = linspace(10e-3, 35e-3, 251);
[xi, zi] = meshgrid(x, z);
M = dasmtx(1i*size(IQ), xi, zi, param, 'nearest');
IQb = M*reshape(IQ, [], 32);
IQb = reshape(IQb, [size(xi) 32]);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Color Doppler

Vd = iq2doppler(IQb, param, [1, 1]);
%Vd = iq2doppler(IQb, param, [11 11]);  % Uncomment to test the smoothing


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Power Doppler

P = sum(abs(IQb).^2, 3);
P = 10*log10(P / max(P(:)));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Display

imagesc(xi(1,:)*100, zi(:,1)*100, P)
caxis([-40 0]) % dynamic range = [-40,0] dB
c = colorbar;
c.YTickLabel{end} = '0 dB';
colormap hot
title('Power Doppler map of the rotating disk')
ylabel('[cm]')
axis equal ij tight
set(gca,'XColor','none','box','off')

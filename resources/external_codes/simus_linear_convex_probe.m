%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Settings

% - linear: pick the L11-5v linear probe
% - convex: pick the C5-2v convex probe
geometry = 'linear';
% - single-scat: One scatterer, centered
% - many-scat: One scatterer in negative lateral, + four in positive
% - many-random-scat: 20 random scatterers, with random amplitudes
medium = 'single-scat';
% - 1pw: Only one plane wave is shot
% - 5pw: Five steered plane waves are shot
compounding = '1pw';


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load a probe

if strcmp(geometry, 'linear')
    probe_name = 'L11-5v';
elseif strcmp(geometry, 'convex')
    probe_name = 'C5-2v';
end
param = getparam(probe_name);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Build a medium

if strcmp(medium, 'single-scat')
    nscat = 1;
    x = [0]; y = [0]; z = [25e-3];
    amp = ones(1, nscat);
elseif strcmp(medium, 'many-scat')
    nscat = 5;
    x = [-5e-3,     0,  2e-3,  4e-3,  6e-3];
    y = [    0,     0,     0,     0,     0];
    z = [20e-3, 30e-3, 30e-3, 30e-3, 30e-3];
    amp = ones(1, nscat);
elseif strcmp(medium, 'many-random-scat')
    nscat = 20;
    x = rand(1, nscat) * 38e-3 - 19e-3;
    y = zeros(1, nscat);
    z = rand(1, nscat) * 20e-3 + 15e-3;
    amp = 1/2 + rand(1, nscat) / 2;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Compute the delays of the plane wave

param.fs = 4*param.fc;
if strcmp(compounding, '1pw')
    angles = [0];
elseif strcmp(compounding, '5pw')
    angles = [-10, -5, 0, 5, 10];
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Simulation

nb_a = size(angles, 2);
delays = txdelay(param, deg2rad(angles));
RF = cell(nb_a, 1);
Nts = zeros(1, nb_a);
for k = 1:nb_a
    RF{k} = simus(x, y, z, amp, delays(k, :), param);
    Nts(k) = size(RF{k}, 1);
end
nt = max(Nts);
for angle = 1:nb_a
    [nnt, nne] = size(RF{angle});
    RF{angle} = cat(1, RF{angle}, zeros(nt - nnt, nne));
end
RF = cat(3, RF{:});


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Save

param.probe_name = probe_name;
param.delays = delays;
name = strcat('simu_', geometry, '_', medium, '_', compounding, '.mat');
save(name, 'RF', 'param');



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DAS

xi = linspace(-20e-3, 20e-3, 512);
zi = linspace(5e-3, 40e-3, 512);
[xi,zi] = meshgrid(xi, zi);
param.fnumber = 1;
IQ = rf2iq(RF, param.fs, param.fc);
IQb = zeros(512, 512, nb_a);
for k = 1:nb_a
    dels = param.delays(k, :);
    IQb(:, :, k) = das(squeeze(IQ(:, :, k)), xi, zi, dels, param, 'nearest');
end
IQb = sum(IQb, 3);
B = bmode(IQb, 60);
imagesc(xi(1, :)*1e2, zi(:, 1)*1e2, B)
c = colorbar;
c.YTick = [0 255];
c.YTickLabel = {'-60 dB','0 dB'};
colormap gray
axis equal tight
xlabel('[cm]'), ylabel('[cm]')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Init the positions
is = zeros(1, 256);
nbs = zeros(1, 4);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Random position if the board is not full

for i=1:256
    while 1
        ri = randi(4);
        if ~(nbs(ri) == 64)
            break;
        end
    end
    is(i) = i + 256 * (ri-1);
    nbs(ri) = nbs(ri) + 1;
end
"""Test different metrics on our data.
"""
import numpy as np
import matplotlib.pyplot as plt

from ultraspy.io.reader import Reader
from ultraspy.scan import GridScan
from ultraspy.beamformers.das import DelayAndSum
import ultraspy as us


# Read real data (from PA2 system)
reader = Reader('/path/to/pa2_wire.mat', 'dbsas')
first_frame = reader.data[0]

# Show raw signals before pre-processing
plt.imshow(first_frame[0], aspect='auto', clim=[0, 1000])
plt.title('Raw data')
plt.show()

# Try to define the signal and noise boundaries for the 64th probe element
# (middle of the probe)
bounds = us.metrics.find_signal_and_noise(first_frame[0, 64], show=True)

# Same, but skipping the first 1000 time samples
bounds = us.metrics.find_signal_and_noise(first_frame[0, 64],
                                          ignore_until=1000, show=True)

snr = us.metrics.signal_noise_ratio(first_frame[0, 64], *bounds)
print(f'SNR: {snr}')

# The central frequency of the probe was ~5MHz, so a band-pass filter
# between 2 and 8MHz should provide good results
sampling_freq = reader.acquisition_info['sampling_freq']
filtered = first_frame.copy()
filtered = us.cpu.filtfilt(filtered, 2e6, sampling_freq, 'high')
filtered = us.cpu.filtfilt(filtered, 8e6, sampling_freq, 'low')

# Let's check the SNR of the filtered data
snr = us.metrics.signal_noise_ratio(filtered[0, 64], *bounds)
print(f'SNR: {snr}')

# Visualize spectra
sampling_freq = reader.acquisition_info['sampling_freq']
frequencies, spectrum_raw = us.cpu.get_spectrum(first_frame[0], sampling_freq)
_, spectrum_filtered = us.cpu.get_spectrum(filtered[0], sampling_freq)
frequencies *= 1e-6  # Convert to MHz

fig, axes = plt.subplots(1, 2)
extent = [frequencies[0], frequencies[-1], 128, 1]
axes[0].imshow(spectrum_raw, aspect='auto', clim=[-40, 0], extent=extent)
axes[0].set_xlabel('Frequencies (MHz)')
axes[0].set_ylabel('Probe elements')
axes[0].set_title('Spectra of raw data')
im2 = axes[1].imshow(spectrum_filtered, aspect='auto', clim=[-40, 0],
                     extent=extent)
axes[1].set_xlabel('Frequencies (MHz)')
axes[1].set_title('Spectra of filtered data')
fig.colorbar(im2, ax=axes[1])
plt.show()

# DAS Beamformer, we are running it on CPU (all the data will be of type
# numpy arrays instead of cupy, so we need to tell him with on_gpu=False)
beamformer = DelayAndSum(on_gpu=False)
beamformer.automatic_setup(reader.acquisition_info, reader.probe)
beamformer.update_setup('f_number', 1.)
beamformer.update_setup('sound_speed', 1450)
beamformer.update_setup('signal_duration', 0.6 * 1e-6)
x = np.linspace(-15, 15, 500) * 1e-3
z = np.linspace(20, 70, 1000) * 1e-3
scan = GridScan(x, z)

# Beamforming, then compute the envelope of the beamformed signal
output = beamformer.beamform(first_frame, scan)
envelope = beamformer.compute_envelope(output, scan)
b_mode = us.cpu.to_b_mode(envelope)

focus_point = us.metrics.get_most_salient_point(b_mode)
lobe_metrics = us.metrics.get_lobe_metrics(b_mode, focus_point, x, z, show=True)
print("Lateral:\n"
      f"\tFWHM: {lobe_metrics['lateral_fwhm']}\n"
      f"\tPSL: {lobe_metrics['lateral_psl']}\n"
      "Axial:\n"
      f"\tFWHM: {lobe_metrics['axial_fwhm']}\n"
      f"\tPSL: {lobe_metrics['axial_psl']}\n")

# Read MUST data (rotating disk)
reader = Reader('/path/to/rotating_disk.mat', 'must')
first_frame = reader.data[0]

# Adjust our beamformer, no need to create a new one
beamformer.set_is_iq(True)
beamformer.automatic_setup(reader.acquisition_info, reader.probe)
beamformer.update_setup('signal_duration', 0)
x = np.linspace(-12.5, 12.5, 250) * 1e-3
z = np.linspace(10, 35, 250) * 1e-3
scan = GridScan(x, z)

# We're working on I/Qs
info = beamformer.setups
iqs = us.cpu.rf2iq(first_frame, info['central_freq'], info['sampling_freq'],
                   beamformer.t0)
output = beamformer.beamform(iqs, scan)
envelope = beamformer.compute_envelope(output, scan)
b_mode = us.cpu.to_b_mode(envelope)

# We define the area of interest. Here a circle is good as we are willing
# to observe the rotating disk. Since we know its center and radius (in mm)
# it is easy to build its mask:
center = (-1e-3, 22.5e-3)
disk_radius = 10e-3
disk_mask = us.metrics.build_mask(center, disk_radius, x, z, 'circle')

# Then we define a noise area, here an empty circle seems good, as it will
# be as close as possible to our interest area. It could also be a
# rectangle or whatever tho.
noise_radius = 12.5e-3
noise_offset = 2e-3
noise_mask = us.metrics.build_mask(center, (noise_radius, noise_offset),
                                   x, z, 'empty_circle')

# Let's visualize our masks
extent = [x * 1e3 for x in [x[0], x[-1], z[-1], z[0]]]  # In mm
b_mode_args = {'extent': extent, 'cmap': 'gray', 'clim': [-60, 0]}
fig, axs = plt.subplots(1, 3)
axs[0].imshow(b_mode.T, **b_mode_args)
axs[1].imshow(np.ma.masked_where(disk_mask, b_mode.T), **b_mode_args)
axs[2].imshow(np.ma.masked_where(noise_mask, b_mode.T), **b_mode_args)
plt.show()

# And finally compute the CNR
cnr = us.metrics.get_contrat_noise_ratio(b_mode.T, disk_mask, noise_mask)
print(f'CNR: {cnr}')

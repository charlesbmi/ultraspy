"""Routine to perform a simple DelayAndSum beamforming on some data from PICMUS
challenge.
"""
import os
import numpy as np

from ultraspy.io.reader import Reader
from ultraspy.scan import GridScan
from ultraspy.beamformers.das import DelayAndSum
import ultraspy as us


# Read PICMUS data
picmus_file = os.path.join('..', 'resources', 'picmus_simu_rfs.hdf5')
reader = Reader(picmus_file, 'picmus')
first_frame = reader.data[0]

# Zone of interest
x = np.linspace(-20, 20, 500) * 1e-3
z = np.linspace(5, 50, 1000) * 1e-3
scan = GridScan(x, z)

# DAS Beamformer
beamformer = DelayAndSum()

# Set the information about the acquisition (probe, angles, ...) and the
# options to use (here the transmit method, which needs to be centered for
# PICMUS data).
beamformer.automatic_setup(reader.acquisition_info, reader.probe)

# Additional parameters
beamformer.update_setup('f_number', 1.75)

# Actual beamform, then compute the envelope of the beamformed signal
d_output = beamformer.beamform(first_frame, scan)
d_envelope = beamformer.compute_envelope(d_output, scan)

# Get the B-Mode to display
us.to_b_mode(d_envelope)
b_mode = d_envelope.get()

# Actual display
import matplotlib.pyplot as plt
extent = [x * 1e3 for x in [x[0], x[-1], z[-1], z[0]]]  # In mm
plt.imshow(b_mode.T, extent=extent, cmap='gray', clim=[-60, 0])
plt.title('DAS on PICMUS - 75 plane waves')
plt.xlabel('Axial (mm)')
plt.ylabel('Depth (mm)')
plt.show()
